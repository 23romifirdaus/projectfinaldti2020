import React, {} from 'react';
import {Text,Image,StyleSheet, SafeAreaView,View, Button} from 'react-native'

const Invoice = ({navigation}) => {
    return(
        <SafeAreaView>
            <View style={{marginTop: 20,marginLeft: 30}}>
                <Text style={{fontSize: 24}}>Invoice Pembelian</Text>
                <Text>Invoice ini merupakan bukti pembayaran yang sah dan diterbitkan oleh Freshmart.</Text>
                <Text style={{marginTop:10}}>No Pesanan : 001301120MFS</Text>
                <Text>Tanggal : 30 Nov 2020 10:30:50</Text>
            </View>
            <View style={{borderRadius: 10, backgroundColor: '#4FABC8',marginTop:20,marginLeft:20,marginRight:20}}>
                    <Text style={{textAlign: 'center',padding:10}}>
                        Tanggal Pesan 30/11/2020
                    </Text>
                    <Text style={{marginLeft: 10}}>
                        Status : Transaksi Berhasil
                    </Text>
                    <Text style={{marginLeft: 10}}>
                        Alamat : Jalan Manggis No. 18 RT 01/RW 08, Kraton, Tegal Barat, Kota Tegal, Jawa Tengah
                    </Text>
                    <Text style={{margin: 10}}>
                        Daftar Barang
                    </Text>
                    <View style={{flexDirection: 'row', justifyContent: 'center',padding:10}}>
                    <Text style={{borderWidth: 1,padding: 2}}>
                        Ikan Tuna Besar
                    </Text>
                    <Text style={{borderWidth: 1,padding: 2,borderLeftWidth:0}}>
                       4 Kg
                    </Text>
                    <Text style={{borderWidth: 1,padding: 2,borderLeftWidth:0}}>
                        Rp 80.000
                    </Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <Text style={{borderWidth: 1,padding: 2}}>
                            Ikan Tuna Besar
                        </Text>
                        <Text style={{borderWidth: 1,padding: 2,borderLeftWidth:0}}>
                        4 Kg
                        </Text>
                        <Text style={{borderWidth: 1,padding: 2,borderLeftWidth:0}}>
                            Rp 80.000
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <Text style={{marginTop:10,marginLeft:10}}>Total Bayar :</Text>
                        <Text style={{marginTop:10,marginLeft:10}}>Rp 160.000</Text>
                    </View>
                    <Text style={{textAlign: 'center',fontSize: 24}}> Lunas</Text>
                
            </View>
            <View style={{marginTop: 10,marginRight:30,marginLeft:30}}>
            <Button
                    title="Kembali"
                    color='#4FABC8'
                    onPress={() => navigation.goBack()}
                />
            </View>
        </SafeAreaView>
    )
}

export default Invoice;