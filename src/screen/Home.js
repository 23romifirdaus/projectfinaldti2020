// In App.js in a new project

import * as React from 'react';
import Axios from 'axios';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  SafeAreaViewBase,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TouchableHighlight } from 'react-native-gesture-handler';
import CardView from 'react-native-cardview';

const Home = ({ navigation }) => {

  return (
    
    <View>
      <View style={{
        backgroundColor: "#4FABC8",
        height: 80,
        borderBottomEndRadius:30,
        borderBottomLeftRadius:30
      }}>
      <Text style={{color:"white", fontSize: 38,textAlign: "center", fontWeight: "bold"}}>Fresh Mart</Text>
      <Text style={{color:"white", fontSize: 20, fontWeight: "bold", paddingLeft:20}}>Hai Wahyu..</Text>

      </View>
      <ScrollView>
      <View style={{flexDirection: "row"}}>
        <TextInput
          placeholder="Cari barang disini.."
          style={{
            height: 40, 
            width: 200,
            borderColor: '#4FABC8', 
            borderWidth: 2,
            borderBottomWidth: 2,
            textAlign:'center',
            fontSize:18,
            marginRight: 10,
            marginLeft: 30,
            marginTop: 20,
            borderRadius: 20
          }}
          />
          <TouchableOpacity onPress={() => navigation.navigate('CariBarang')}>
          <Text style={{
          borderColor: '#4FABC8', 
          borderWidth: 2,
          height: 40,
          width: 80,
          color: '#8B8282',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 20,
          fontSize:18,
          borderRadius: 20,
          marginLeft: 18,
          marginRight: 18}}>
          Cari
          </Text>
          </TouchableOpacity>
      </View>
        <Text style={{marginTop: 20, marginBottom: -20, marginLeft: 20, fontSize: 24, fontWeight: "bold" , color: '#4FABC8'}}>Barang Tersedia</Text>
        <View style={{flexDirection: "row"}}>
          <View style={{ marginHorizontal: 15,marginTop: 30, backgroundColor: "#EDDBDB", borderRadius: 20}}>
          <Image source={require('../img/IkanTuna.png')} style={{borderColor:'#4FABC8', borderWidth:2, borderRadius:18}}/>
          <Text style={{padding: 5,textAlign: "left", marginLeft:20}}>Ikan Tuna Besar {"\n"}
          Rp 20.000/Kg{"\n"}
          Stok 100kg {"\n"}
          Diambil Dari : {"\n"}
          Pelabuhan Tegal
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('DetailProduk')}>
          <Text style={{
          backgroundColor: '#53BD64',
          height: 30,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          borderRadius: 10,
          marginLeft: 18,
          marginRight: 18}}>
          Tambah Keranjang
          </Text>
          </TouchableOpacity>
        <View style={{flexDirection: "row"}}>
        <TextInput
          placeholder="kg"
          style={{
            height : 35, 
            width: 75,
            textAlign:'center',
            borderColor: '#4FABC8', 
            borderWidth: 2,
            borderBottomWidth: 2,
            marginTop: 10,
            borderTopLeftRadius: 10,
            borderBottomLeftRadius:10,
            marginLeft: 18,
            marginBottom:10
          }}
          />
          <TouchableOpacity onPress={() => navigation.navigate('Pemesanan')}>
          <Text style={{
          backgroundColor: '#4FABC8',
          height: 35,
          width : 45,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          marginBottom: 10,
          borderTopRightRadius: 10,
          borderBottomRightRadius:10,
          marginRight: 18}}>
          Pesan
          </Text>
          </TouchableOpacity>
          </View>
          </View>
          <View style={{ marginHorizontal: 5,marginTop: 30, backgroundColor: "#EDDBDB", borderRadius: 20}}>
          <Image source={require('../img/IkanTuna.png')} style={{borderColor:'#4FABC8', borderWidth:2, borderRadius:18}}/>
          <Text style={{padding: 5,textAlign: "left", marginLeft:20}}>Ikan Tuna Medium {"\n"}
          Rp 18.000/Kg{"\n"}
          Stok 100kg {"\n"}
          Diambil Dari : {"\n"}
          Pelabuhan Tegal
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('DetailProduk')}>
          <Text style={{
          backgroundColor: '#53BD64',
          height: 30,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          borderRadius: 10,
          marginLeft: 18,
          marginRight: 18}}>
          Tambah Keranjang
          </Text>
          </TouchableOpacity>
          <View style={{flexDirection: "row"}}>
        <TextInput
          placeholder="kg"
          style={{
            height : 35, 
            width: 75,
            borderColor: '#4FABC8', 
            textAlign:'center',
            borderWidth: 2,
            borderBottomWidth: 2,
            marginTop: 10,
            borderTopLeftRadius: 10,
            borderBottomLeftRadius:10,
            marginLeft: 18,
            marginBottom:10
          }}
          />
          <TouchableOpacity onPress={() => navigation.navigate('KeranjangBelanja')}>
          <Text style={{
          backgroundColor: '#4FABC8',
          height: 35,
          width : 45,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          marginBottom: 10,
          borderTopRightRadius: 10,
          borderBottomRightRadius:10,
          marginRight: 18}}>
          Pesan
          </Text>
          </TouchableOpacity>
          </View>
          </View>
          </View>
          <View style={{flexDirection: "row", paddingBottom: 100}}>
          <View style={{ marginHorizontal: 15,marginTop: 20, backgroundColor: "#EDDBDB", borderRadius: 20}}>
          <Image source={require('../img/IkanTuna.png')} style={{borderColor:'#4FABC8', borderWidth:2, borderRadius:18}}/>
          <Text style={{padding: 5,textAlign: "left", marginLeft:20}}>Ikan Tuna Medium {"\n"}
          Rp 18.000/Kg{"\n"}
          Stok 100kg {"\n"}
          Diambil Dari : {"\n"}
          Pelabuhan Tegal
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('DetailProduk')}>
          <Text style={{
          backgroundColor: '#53BD64',
          height: 30,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          borderRadius: 10,
          marginLeft: 18,
          marginRight: 18}}>
          Tambah Keranjang
          </Text>
          </TouchableOpacity>
          <View style={{flexDirection: "row"}}>
        <TextInput
          placeholder="kg"
          style={{
            height : 35, 
            width: 75,
            borderColor: '#4FABC8', 
            borderWidth: 2,
            borderBottomWidth: 2,
            textAlign:'center',
            marginTop: 10,
            borderTopLeftRadius: 10,
            borderBottomLeftRadius:10,
            marginLeft: 18,
            marginBottom:10
          }}
          />
          <TouchableOpacity onPress={() => navigation.navigate('Pemesanan')}>
          <Text style={{
          backgroundColor: '#4FABC8',
          height: 35,
          width : 45,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          marginBottom: 10,
          borderTopRightRadius: 10,
          borderBottomRightRadius:10,
          marginRight: 18}}>
          Pesan
          </Text>
          </TouchableOpacity>
          </View>
          </View>
          <View style={{ marginHorizontal: 5,marginTop: 20, backgroundColor: "#EDDBDB", borderRadius: 20}}>
          <Image source={require('../img/IkanTuna.png')} style={{borderColor:'#4FABC8', borderWidth:2, borderRadius:18}}/>
          <Text style={{padding: 5,textAlign: "left", marginLeft:20}}>Ikan Tuna Medium {"\n"}
          Rp 18.000/Kg{"\n"}
          Stok 100kg {"\n"}
          Diambil Dari : {"\n"}
          Pelabuhan Tegal
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('DetailProduk')}>
          <Text style={{
          backgroundColor: '#53BD64',
          height: 30,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          borderRadius: 10,
          marginLeft: 18,
          marginRight: 18}}>
         Tambah Keranjang
          </Text>
          </TouchableOpacity>
          <View style={{flexDirection: "row"}}>
        <TextInput
          placeholder="kg"
          style={{
            height : 35, 
            width: 75,
            borderColor: '#4FABC8', 
            textAlign:'center',
            borderWidth: 2,
            borderBottomWidth: 2,
            marginTop: 10,
            borderTopLeftRadius: 10,
            borderBottomLeftRadius:10,
            marginLeft: 18,
            marginBottom:10
          }}
          />
          <TouchableOpacity onPress={() => navigation.navigate('Pemesanan')}>
          <Text style={{
          backgroundColor: '#4FABC8',
          height: 35,
          width : 45,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          marginBottom: 10,
          borderTopRightRadius: 10,
          borderBottomRightRadius:10,
          marginRight: 18}}>
          Pesan
          </Text>
          </TouchableOpacity>
          </View>
          </View>
          </View>
    </ScrollView>
    </View>
  );
}

export default Home;