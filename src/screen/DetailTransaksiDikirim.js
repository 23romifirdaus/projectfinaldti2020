import React, {} from 'react';
import {Text,Image,StyleSheet, SafeAreaView,View, Button} from 'react-native'

const DetailTransaksiDikirim = ({navigation}) => {
    return(
        <SafeAreaView>
            <View style={{alignItems: 'center',marginTop: 20}}>
                <Image source={require('../img/deliveryman.png')}/>
                <Text style={{textAlign: 'center',fontSize: 24}}>Pesanan Sedang Dikirim</Text>
            </View>
            <View style={{borderRadius: 10, backgroundColor: '#4FABC8',marginTop:20,marginLeft:20,marginRight:20}}>
                    <Text style={{textAlign: 'center',padding:10}}>
                        Tanggal Pesan 30/11/2020
                    </Text>
                    <Text style={{marginLeft: 10}}>
                        Nomor Pemesanan : 001301120MFS
                    </Text>
                    <Text style={{marginLeft: 10}}>
                        Alamat : Jalan Manggis No. 18 RT 01/RW 08, Kraton, Tegal Barat, Kota Tegal, Jawa Tengah
                    </Text>
                    <Text style={{margin: 10}}>
                        Daftar Barang
                    </Text>
                    <View style={{flexDirection: 'row', justifyContent: 'center',padding:10}}>
                    <Text style={{borderWidth: 1,padding: 2}}>
                        Ikan Tuna Besar
                    </Text>
                    <Text style={{borderWidth: 1,padding: 2,borderLeftWidth:0}}>
                       4 Kg
                    </Text>
                    <Text style={{borderWidth: 1,padding: 2,borderLeftWidth:0}}>
                        Rp 80.000
                    </Text>
                    </View>
                    
                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <Text style={{marginTop:10,marginLeft:10}}>Total Bayar :</Text>
                        <Text style={{marginTop:10,marginLeft:10}}>Rp 80.000</Text>
                    </View>
                    <Text style={{textAlign: 'center',fontSize: 24}}>Belum Lunas</Text>
                
            </View>
            <View style={{margin: 20}}>
            <Button
                    title="Kembali"
                    color='#4FABC8'
                    onPress={() => navigation.navigate('Home')}
                />
            </View>
        </SafeAreaView>
    )
}

export default DetailTransaksiDikirim;