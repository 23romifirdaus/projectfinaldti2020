import React, { useEffect } from 'react'
import { StyleSheet, Image, Text, View } from 'react-native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
        navigation.replace('Login');
    }, 3000);
  }, []);

  return (
    <View style={styles.page}>
        <Image
            style={{
              width: 140,
              height: 200,
              marginBottom: -60,
            }}
            source={require('../../src/img/logo.png')} 
          />
      <Text style={styles.title2}>
        Agro Bahari
      </Text>
      <Text style={styles.title}>
        FreshMart
      </Text>
    </View>
  )
}

export default Splash;

const styles = StyleSheet.create({
  page: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: '#4FABC8'
  },
  title: {
    fontSize: 35,
    fontWeight: "600",
    color: "#FFFFFF",
    textAlign: "center"
  },
  title2: {
    fontSize: 30,
    fontWeight: "600",
    color: "#FFFFFF",
    textAlign: "center"
  },
});