import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const Alamat = () => {
    return (
        <View>
            <View>
            <TouchableOpacity>
            <Text style={{
            textAlign: 'center', 
            backgroundColor: '#4FABC8',
            padding: 10,
            margin: 40,
            color: 'white',
            borderRadius: 30
            }}>Tambah Alamat</Text> 
            </TouchableOpacity>

            <Text style={{fontSize: 24, color: '#4FABC8', fontWeight: "bold"}}> Alamat Saat ini :</Text>
            </View>
            <View style={{backgroundColor: '#4FABC8', marginVertical: 20, marginHorizontal: 20, paddingBottom: 10}}>
                <Text style={{textAlign: "center", fontSize: 21}}>Alamat 1</Text>
                <Text style={{textAlign: "left", marginHorizontal: 10, fontSize: 18, marginTop: 10, marginBottom: 10}}>
                Jalan Manggis No. 16, Kelurahan Kraton, Kecamatan Tegal Barat ,Kota Tegal
                </Text>
                <TouchableOpacity>
                    <Text style={{marginHorizontal:10, borderRadius: 20,backgroundColor: "#53BD64", textAlign: "center", padding: 5}}>Ubah</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Alamat
