// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


const KeranjangKosong = ({ navigation }) => {
    return (
        <View>
            <View style={{
                backgroundColor: "#4FABC8",
                height: 60,
            }}>
                <Text style={{color:"white", fontSize: 32, marginLeft: 40, marginTop: 10, textAlign: "left", fontWeight: "bold"}}>Keranjang Belanja</Text>
            </View>
            <View style={styles.page}>
                
                <Image
                    style={{
                    width: 240,
                    height: 200,
                    marginTop: 400,
                    }}
                    source={require('../../src/img/icon_cart2.png')} 
                />
            <Text style={styles.title}>
                Keranjang belanja Anda masih kosong
            </Text>
            </View>
            <View style={{
                alignItems: 'center', 
                justifyContent: 'center',
            }}>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
                <Text 
                style={{
                backgroundColor: '#4FABC8',
                height: 40,
                width: 300,
                color: '#fff',
                textAlign: "center",
                fontSize: 24,
                textAlignVertical: "center",
                marginTop: 320,
                borderRadius:10,
                }}>Belanja Sekarang</Text>
            </TouchableOpacity>
            </View>
        </View>
      )
    }
    
    export default KeranjangKosong;
    
    const styles = StyleSheet.create({
      page: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: '#FFFFFF'
      },
      title: {
        fontSize: 20,
        fontWeight: "600",
        color: "black",
        textAlign: "center"
      },
    });