import React from 'react'
import { StyleSheet, Text, View , TextInput, Button} from 'react-native'
import DetailTransaksiDibatalkan from './DetailTransaksiDibatalkan'

const BatalkanPesanan = ({navigation}) => {
    return (
        <View style={{marginTop: 100, backgroundColor: '#4FABC8',borderRadius: 10}}>
        <View>
            <Text style={{textAlign: "center", margin:20}}>Pembatalan Pesanan</Text>
        </View>
        <TextInput style={{borderWidth: 1, margin: 10, color: 'grey', borderColor: 'grey',backgroundColor: 'white',borderRadius: 10}}>
            Alasan Pembatalan
        </TextInput>
        <View style={{marginLeft: 50, marginRight: 50, marginBottom: 10}}>
        <Button onPress={() => navigation.navigate(DetailTransaksiDibatalkan)}
        title="Batalkan Pesanan"
        color="#53BD64" />
        </View>
        </View>
    )
}

export default BatalkanPesanan
