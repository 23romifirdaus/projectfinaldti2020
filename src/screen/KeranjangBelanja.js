// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


const KeranjangBelanja = ({ navigation }) => {
    return (
        <View>
            <View style={{
                backgroundColor: "#4FABC8",
                height: 60,
            }}>
                <Text style={{color:"white", fontSize: 32, marginLeft: 40, marginTop: 10, textAlign: "left", fontWeight: "bold"}}>Keranjang Belanja</Text>
            </View>
            
        <ScrollView>
            <View style={{
                    backgroundColor: "#4FABC8",
                    height: 40,
                    marginLeft: 30,
                    marginTop: 20,
                    alignItems: 'center', 
                    justifyContent: 'center',
                    width: 300,
                    borderRadius:10,
                }}>
                    <Text style={{color:"white", fontSize: 24, marginLeft: 0, marginTop: 0, textAlign: "center", fontWeight: "bold"}}>Keranjang Belanja Anda</Text>
                </View>
                <View style={{
                    alignItems: 'center', 
                    justifyContent: 'center',
                }}>
                <Text style={{color:"black", fontSize: 20, marginLeft: 0, marginTop: 10, textAlign: "center", fontWeight: "bold"}}>Berhasil menambahkan Ikan Tuna Besar</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
                <Text 
                style={{
                backgroundColor: '#53BD64',
                height: 40,
                width: 300,
                color: '#fff',
                textAlign: "center",
                fontSize: 18,
                borderRadius:10,
                textAlignVertical: "center",
                marginTop: 10,
                }}>Kosongkan Keranjang</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
                <Text 
                style={{
                backgroundColor: '#53BD64',
                height: 40,
                width: 300,
                borderRadius:10,
                color: '#fff',
                textAlign: "center",
                fontSize: 18,
                textAlignVertical: "center",
                marginTop: 8,
                }}>Tambah Barang</Text>
            </TouchableOpacity>
            </View>
                <View style={{
                        backgroundColor: "#4FABC8",
                        height: 240,
                        marginLeft: 20,
                        marginTop: 20,
                        alignItems: 'center', 
                        justifyContent: 'center',
                        width: 320,
                        borderRadius:10,
                    }}>
                        
                        <View style={{flexDirection: "row", marginBottom: 20}}>
                        <View style={{
                        marginLeft: 20,
                        }}>
                             <Image source={require('../img/IkanTuna.png')} style={{borderColor:'#4FABC8', borderWidth:2, borderRadius:18, marginLeft:-10}}/>
                             <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
                                <Text 
                                style={{
                                backgroundColor: '#53BD64',
                                height: 40,
                                width: 180,
                                color: '#fff',
                                textAlign: "center",
                                fontSize: 18,
                                textAlignVertical: "center",
                                marginTop: 10,
                                }}>Hapus Barang</Text>
                            </TouchableOpacity>
                            <Text style={{
                        fontSize: 18,
                        marginLeft: 0,
                        marginTop: 20,
                        color: "white",
                        fontWeight: "bold"
                        }}> Sub Total Harga</Text>
                        </View>
                        <View>
                        <Text style={{
                        fontSize: 18,
                        marginLeft: -25,
                        marginTop: 20,
                        color: "white",
                        }}>Ikan Tuna Besar  {"\n"}
                            Rp. 20.000/Kg  </Text>   
                            <TextInput
                        placeholder= "4 kg"
                        style={{
                            height : 40, 
                            width: 75,
                            backgroundColor: 'white',
                            textAlign:'center',
                            marginLeft: 4,
                            marginTop:45
                        }}
                        /> 
                        <Text style={{
                        fontSize: 18,
                        marginLeft: -16,
                        marginTop: 20,
                        color: "white",
                        fontWeight: "bold"
                        }}> Rp. 80.000</Text>
                        </View>
                        
                    </View>
                </View>
                <View style={{
                        backgroundColor: "#4FABC8",
                        height: 240,
                        marginLeft: 20,
                        marginTop: 20,
                        alignItems: 'center', 
                        justifyContent: 'center',
                        width: 320,
                        borderRadius:10,
                    }}>
                        
                        <View style={{flexDirection: "row", marginBottom: 20}}>
                        <View style={{
                        marginLeft: 20,
                        }}>
                             <Image source={require('../img/IkanTuna.png')} style={{borderColor:'#4FABC8', borderWidth:2, borderRadius:18, marginLeft:-10}}/>
                             <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
                                <Text 
                                style={{
                                backgroundColor: '#53BD64',
                                height: 40,
                                width: 180,
                                color: '#fff',
                                textAlign: "center",
                                fontSize: 18,
                                textAlignVertical: "center",
                                marginTop: 10,
                                }}>Hapus Barang</Text>
                            </TouchableOpacity>
                            <Text style={{
                        fontSize: 18,
                        marginLeft: 0,
                        marginTop: 20,
                        color: "white",
                        fontWeight: "bold"
                        }}> Sub Total Harga</Text>
                        </View>
                        <View>
                        <Text style={{
                        fontSize: 18,
                        marginLeft: -25,
                        marginTop: 20,
                        color: "white",
                        }}>Ikan Tuna Medium  {"\n"}
                            Rp. 18.000/Kg  </Text>   
                            <TextInput
                        placeholder= "2 kg"
                        style={{
                            height : 40, 
                            width: 75,
                            backgroundColor: 'white',
                            textAlign:'center',
                            marginLeft: 4,
                            marginTop:45
                        }}
                        /> 
                        <Text style={{
                        fontSize: 18,
                        marginLeft: -16,
                        marginTop: 20,
                        color: "white",
                        fontWeight: "bold"
                        }}> Rp. 36.000</Text>
                        </View>
                        
                    </View>
                    
                </View>
                <View style={{
                        backgroundColor: "#4FABC8",
                        height: 140,
                        marginLeft: 20,
                        marginTop: 20,
                        alignItems: 'center', 
                        justifyContent: 'center',
                        width: 320,
                        borderRadius:10,
                    }}></View>
                </ScrollView>
                <View style={{position: 'absolute',backgroundColor:'white', height:150, left: 0, right: 0, bottom: 0}}>
                    <View style={{flexDirection: "row", marginBottom: 20}}>
                        <Text style={{
                        fontSize: 22,
                        marginLeft: 20,
                        marginTop: 10,
                        color: 'black',
                        fontWeight: "bold"
                        }}> Total Harga
                        </Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Pengiriman')}>      
                            <Text 
                            style={{
                            backgroundColor: '#53BD64',
                            height: 60,
                            width: 160,
                            color: '#fff',
                            textAlign: "center",
                            fontSize: 24,
                            textAlignVertical: "center",
                            marginLeft: 50,
                            marginTop: 10,
                            borderRadius: 10,
                            }}>Pilih Pengiriman</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{
                        fontSize: 24,
                        marginLeft: 20,
                        marginTop: -50,
                        color: 'black',
                        fontWeight: "bold"
                        }}> Rp. 116.000
                        </Text>
                </View>
        </View>
        
      )
    }
    
    export default KeranjangBelanja;
    