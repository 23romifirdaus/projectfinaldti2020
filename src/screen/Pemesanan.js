// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CardView from 'react-native-cardview'


const Pemesanan = ({ navigation }) => {
  return (
      <View>
          <View>
              <Text style={{marginTop: 20,marginLeft: 25,fontSize: 18, fontWeight: "bold"}}>
                  Pesanan Anda
              </Text>
              <Text style={{fontWeight:"bold", fontSize: 48, textAlign: "center" }}>
                  Ikan Tuna Besar
              </Text>
              <Text style={{fontSize: 18, fontWeight: "bold", textAlign: "center"}}>
                  Rp 20.000/Kg
              </Text>
              <Text style={{fontSize: 18, fontWeight: "bold", marginLeft: 24,paddingTop: 20}}>
                  Harap Isi Form Dibawah Ini
              </Text>
                <TextInput
              placeholder="Alamat Lengkap"
              style={{
                height: 40,
                padding: 10,
                marginTop: 10,
                marginBottom: 5,
                marginLeft: 20,
                marginRight: 20,
                borderColor: '#A9A9A9', 
                borderWidth: 1,
                borderBottomWidth: 1,
                borderRadius: 7
                }}/>
                <TextInput
              placeholder="Nomor Handphone"
              keyboardType="numeric"
              style={{
                height: 40,
                padding: 10,
                marginTop: 10,
                marginBottom: 5,
                marginLeft: 20,
                marginRight: 20,
                borderColor: '#A9A9A9', 
                borderWidth: 1,
                borderBottomWidth: 1,
                borderRadius: 7
                }}/>
                 <TextInput
              placeholder="Metode Pembayaran"
              style={{
                height: 40,
                padding: 10,
                marginTop: 10,
                marginBottom: 5,
                marginLeft: 20,
                marginRight: 20,
                borderColor: '#A9A9A9', 
                borderWidth: 1,
                borderBottomWidth: 1,
                borderRadius: 7
                }}/>

                <View style={{flexDirection: "row", marginLeft: 30}}>
                    <Text style={{ marginTop: 20, fontSize: 18, fontWeight: "bold"}}>
                        Jumlah
                    </Text>
                    
                    <TextInput
                        placeholder="Jumlah"
                        keyboardType="numeric"
                        style={{
                            height: 40,
                            width: 70,
                            padding: 10,
                            marginTop: 10,
                            marginBottom: 5,
                            marginLeft: 140,
                            marginRight: 2,
                            borderColor: '#A9A9A9', 
                            borderWidth: 1,
                            borderBottomWidth: 1,
                            borderRadius: 7
                            }}
                    />
                    <Text style={{ marginTop: 20, marginRight: 2, fontSize: 18, fontWeight: "bold"}}>
                        Kg
                    </Text>

                </View>
                <View style={{flexDirection: "row", marginLeft: 30}}>
                    <Text style={{ marginTop: 20, fontSize: 18, fontWeight: "bold"}}>
                        Total Harga
                    </Text>
                    <Text style={{ marginTop: 20,marginLeft: 105 ,fontSize: 18, fontWeight: "bold", backgroundColor: "#4FABC8", borderRadius: 5,padding: 5, color:"white"}}>
                        Rp 40.000
                    </Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('KonfirmasiPemesanan')}>
                    <Text style={{
                        padding: 10,
                        backgroundColor: "#53BD64",
                        borderRadius: 30,
                        color: 'white',
                        marginTop: 10,
                        marginLeft: 30,
                        marginRight: 30,
                        textAlign: "center"
                    }}>
                    Lanjutkan
                    </Text>
                </TouchableOpacity>
                
          </View>
      </View>
  );
}

export default Pemesanan;