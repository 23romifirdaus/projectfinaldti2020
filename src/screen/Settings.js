// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CardView from 'react-native-cardview'

const Settings = ({ navigation }) => {
  return (
      <View style={{
        }}>
          <View style={{
            backgroundColor: '#4FABC8',
            borderBottomLeftRadius:30,
            borderBottomRightRadius:30,}}>
        <Text style={{
         textAlign: 'center',
         fontWeight: 'bold'
        }}></Text>
        <Image style={{alignSelf: "center"}} source={require('../img/background_foto.png')}/>
        <Text style={{
          paddingTop:10,
          marginLeft: 20,
          marginRight:20,
          fontSize:20,
          color:'white',
          textAlign:'center',
        }}>Wahyu Nurrosyid {"\n"}
        0812345678909 {"\n"}
        </Text>  
        </View>
        <View>    
        <TouchableOpacity onPress={() => navigation.navigate('UbahProfile')}>      
          <Text 
          style={{
          backgroundColor: '#4FABC8',
          height: 40,
          color: '#fff',
          textAlign: "center",
          fontSize: 16,
          textAlignVertical: "center",
          marginTop: 20,
          borderRadius: 10,
          marginTop: 10,
          marginLeft: 18,
          marginRight: 18
          }}>Ubah Profile</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('Alamat')}>      
          <Text 
          style={{
          backgroundColor: '#4FABC8',
          height: 40,
          color: '#fff',
          textAlign: "center",
          fontSize: 16,
          textAlignVertical: "center",
          marginTop: 20,
          borderRadius: 10,
          marginTop: 10,
          marginLeft: 18,
          marginRight: 18
          }}>Alamat</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('Keluhan')}>      
          <Text 
          style={{
          backgroundColor: '#4FABC8',
          height: 40,
          color: '#fff',
          textAlign: "center",
          fontSize: 16,
          textAlignVertical: "center",
          marginTop: 20,
          borderRadius: 10,
          marginTop: 10,
          marginLeft: 18,
          marginRight: 18
          }}>Saran dan Kritik</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('GantiPassword')}>      
          <Text 
          style={{
          backgroundColor: '#4FABC8',
          height: 40,
          color: '#fff',
          textAlign: "center",
          fontSize: 16,
          textAlignVertical: "center",
          marginTop: 20,
          borderRadius: 10,
          marginTop: 10,
          marginLeft: 18,
          marginRight: 18
          }}>Ganti Password</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('About')}>      
          <Text 
          style={{
          backgroundColor: '#4FABC8',
          height: 40,
          color: '#fff',
          textAlign: "center",
          fontSize: 16,
          textAlignVertical: "center",
          marginTop: 20,
          borderRadius: 10,
          marginTop: 10,
          marginLeft: 18,
          marginRight: 18
          }}>About</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text
            style={{ 
            color: '#4FABC8',
            fontSize: 16,
            marginTop: 20,
            textAlign: "center"
            }}>Logout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Settings;