// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CardView from 'react-native-cardview'


const KonfirmasiPembayaran = ({ navigation }) => {
  return (
      <View>
          <View>
              <Text style={{textAlign:"center",fontSize:28,marginTop: 20, padding: 20}}>Harap Upload Bukti Transfer</Text>
              <Text style={{marginTop: 20, marginLeft: 60,marginBottom:5}}>
                  Foto Bukti Transfer
              </Text>
              <View style={{}}>
                  <TouchableOpacity style={{padding: 10,}}>
                      <Text style={{color: "white",backgroundColor: "#53BD64", borderRadius:10,marginLeft: 50, marginRight: 50,padding: 10}}>
                      Pilih Foto
                      </Text>
                  </TouchableOpacity>
              </View>
              <TextInput
        placeholder="Internal/mandiri/bukti.jpg"
        style={{
          height: 40,
          borderColor: '#A9A9A9', 
          borderWidth: 1,
          borderBottomWidth: 1,
          marginTop: -5,
          marginLeft: 60,
          marginRight:60,
          marginBottom: 10,
          borderRadius: 7
        }}
        />
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Text style={{ textAlign: "center",backgroundColor:"#53BD64",marginLeft: 50,marginRight: 50,padding: 10,marginTop: 20, borderRadius: 30, color: "white"}}>
                  Upload Sekarang
              </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Text style={{textAlign: "center", marginTop:10}}>
                  Upload Nanti
              </Text>
          </TouchableOpacity>
      </View>
  );
}

export default KonfirmasiPembayaran;