// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CardView from 'react-native-cardview'

const Pembayaran = ({ navigation }) => {
  return (
      <View>
         <View>
             <Text style={{marginTop: 20,marginLeft: 20, marginBottom: 20, fontSize: 18}}>
             Harap transfer ke rekening :
             </Text>
             <View style={{backgroundColor: "#4FABC8", borderRadius: 10, marginLeft: 50, marginRight: 50,padding: 10}}>
                 <Text style={{
                     color: "white",
                     textAlign: "center",
                     fontSize: 24
                 }}>
                    Bank Mandiri{"\n"}
                    139 000 1000 0210{"\n"}
                    PT. Freshmart Jaya
                 </Text>
             </View>
             
            <Text style={{marginTop: 20, fontSize: 16, textAlign: "center"}}>Waktu pembayaran dalam 1 jam</Text>
            <Text style={{marginTop: 10, fontSize: 16, marginLeft:45, marginRight:40}}>Jika pembayaran tidak dilakukan dalam 1 jam, maka pesanan otomatis dibatalkan</Text>
            <TouchableOpacity onPress={() => navigation.navigate('KonfirmasiPembayaran')}>
                <Text style={{marginTop: 20,backgroundColor: "#53BD64", textAlign: "center",marginLeft: 70, marginRight:70, padding: 10, borderRadius: 30, color: "white"}}>Konfirmasi Pembayaran</Text>
            </TouchableOpacity>
        </View>
      </View>
  );
}

export default Pembayaran;