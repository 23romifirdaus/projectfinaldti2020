import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


const DetailPesanan = ({ navigation }) => {
  return (
    <View>
        <View style={{backgroundColor: "#4FABC8", borderRadius: 10, marginLeft: 50, marginTop: 20, marginRight: 50,padding: 10}}>
            <Text style={{
                color: "white",
                textAlign: "left",
                fontSize: 16
            }}>
            Status Pemesanan: Menunggu Pembayaran{"\n"}
            Sisa Waktu Pesan 30 menit{"\n"}
            Np. Pemesanan: 30112020TGLMFS{"\n"}
            Tanggal Pesan: 30/11/2020{"\n"}
            Nama Pemesan: M Fikri S{"\n"}
            Nama Penerima: Romi Firdaus{"\n"}
            Barang Pesan: Ikan Tuna Besar 2 kg{"\n"}
            Total Harga:Rp. 40.000{"\n"}
            Alamat Kirim: Jalan Jakarta, Kecamatan Bandung Timur, Kota Bandung, Jawa Barat{"\n"}
            No. Handphone: 08123456789{"\n"}
            Metode Pembayaran: Transfer Bank Mandiri{"\n"}
            </Text>
        </View>
        <Text 
            style={{
                color: "black",
                textAlign: "justify",
                marginLeft: 20,
                marginRight: 20,
                marginTop: 20,
                fontSize: 16
            }}>
                Mohon untuk lakukan pembayaran, jika dalam waktu 30 menit tidak dibayar maka pesanan otomatis dibatalkan.
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate('CaraPembayaran')}>
          <Text style={{
          backgroundColor: '#53BD64',
          height: 40,
          width: 320,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          marginBottom: 10,
          borderRadius: 6,
          marginLeft: 18,
          marginRight: 18}}>
          Cara Pembayaran
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('KonfirmasiPembayaran')}>
          <Text style={{
          backgroundColor: '#53BD64',
          height: 40,
          width: 320,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 0,
          marginBottom: 10,
          borderRadius: 6,
          marginLeft: 18,
          marginRight: 18}}>
          Upload Bukti Pembayaran
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('BatalkanPesanan')}>
          <Text style={{
          backgroundColor: '#53BD64',
          height: 40,
          width: 320,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 0,
          marginBottom: 10,
          borderRadius: 6,
          marginLeft: 18,
          marginRight: 18}}>
          Batalkan Pesanan
          </Text>
        </TouchableOpacity>
    </View>
  );
}

export default DetailPesanan;