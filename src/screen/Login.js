// In App.js in a new project

import * as React from 'react';
import Axios from 'axios';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TouchableHighlight } from 'react-native-gesture-handler';
import CardView from 'react-native-cardview'

const Login = ({ navigation }) => {
  return (
    <View
        style={{
          paddingTop: 40,
          marginRight: 28,
          marginLeft: 28
        }}
      >
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
        <Image
            style={{
              width: 140,
              height: 200,
              marginBottom: -60,
            }}
            source={require('../../src/img/logo.png')} 
          />
        <Text
          style={{
            fontSize: 40,
            color: '#4FABC8',
          }}
        >
          Fresh Mart
        </Text>
        </View>
        <TextInput
        placeholder="Email"
        style={{
          height: 40,
          borderColor: '#A9A9A9', 
          borderWidth: 1,
          borderBottomWidth: 1,
          marginBottom: 10,
          borderRadius: 7,
          marginTop: 20
        }}
        />
        <TextInput
        placeholder="Password" secureTextEntry
        style={{
          TextInput:"password",
          height: 40, 
          borderColor: '#A9A9A9', 
          borderWidth: 1,
          borderBottomWidth: 1,
          marginBottom: 20,
          borderRadius: 7,
        }}
        />
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
          <Text 
          style={{
          backgroundColor: '#4FABC8',
          height: 40,
          color: '#fff',
          textAlign: "center",
          fontSize: 16,
          textAlignVertical: "center",
          marginTop: 0,
          borderRadius: 10
          }}>Login</Text>
        </TouchableOpacity>
        <Text style={{
          color: 'grey',
          textAlign: "center",
          marginTop: 20,
          fontSize: 16}}>Sudah Mempunyai Akun? Masuk Disini</Text> 
        <TouchableOpacity onPress={() => navigation.navigate('Registrasi')}>
            <Text
            style={{ 
            color: '#4FABC8',
            fontSize: 16,
            marginTop: 10,
            textAlign: "center"
            }}>Daftar Akun</Text>
        </TouchableOpacity>
      </View>
  );
}

export default Login;