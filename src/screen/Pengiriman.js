// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


const Pengiriman = ({ navigation }) => {
    return (
        <View>
            
        <ScrollView>
            <View style={{flexDirection: "row", marginBottom: 20}}>
                <Text style={{
                    color: "#4FABC8",
                    fontSize: 18,
                    marginLeft: 20,
                    marginTop: 15,
                }}>
                    Alamat Pengiriman
                </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
                    <Text 
                    style={{
                    backgroundColor: '#53BD64',
                    height: 40,
                    marginLeft: 10,
                    width: 160,
                    color: '#fff',
                    textAlign: "center",
                    fontSize: 18,
                    textAlignVertical: "center",
                    marginTop: 8,
                    borderRadius:5,
                    }}>Tambah Alamat</Text>
                </TouchableOpacity>
            </View>
            <TextInput
            placeholder="Alamat 1 - Jalan Manggis"
            style={{
                height: 40, 
                borderColor: '#A9A9A9', 
                borderWidth: 1,
                marginLeft: 20,
                marginRight: 20,
                borderBottomWidth: 1,
                marginBottom: 20,
                borderRadius: 7
            }}
            />
            <View style={{
                    backgroundColor: "#4FABC8",
                    height: 160,
                    marginLeft: 30,
                    marginTop: 0,
                    alignItems: 'center', 
                    justifyContent: 'center',
                    width: 300,
                    borderRadius:10,
                }}>
                    <Text style={{color:"white", fontSize: 24, marginTop: -10, marginLeft: 0, marginBottom: 10, fontWeight: "bold"}}>Alamat 1</Text>
                    <Text style={{color:"white", fontSize: 18, marginLeft: -140, marginTop: -6, fontWeight: "bold"}}>Wahyu Nurrosyid</Text>
                    <Text style={{color:"white", fontSize: 18, marginLeft: -165, marginTop: 2, fontWeight: "bold"}}>08123456789</Text>
                    <Text style={{color:"white", fontSize: 18, marginLeft: 10, marginTop: 0, fontWeight: "bold"}}>Jalan Manggis No. 16 RT/RW 001/004, Kraton, Tegal Barat, Kota Tegal, Jawa Tengah</Text>
                </View>
                <Text style={{color:"#4FABC8", fontSize: 16, marginLeft: 20, marginTop: 10, fontWeight: "bold"}}>Pembayaran dilakukan saat barang diterima</Text>
                <View style={{
                    backgroundColor: "#4FABC8",
                    height: 70,
                    marginLeft: 30,
                    marginTop: 0,
                    alignItems: 'center', 
                    justifyContent: 'center',
                    width: 300,
                    borderRadius:10,
                }}>
                    
                    <Text style={{color:"white", fontSize: 18, marginLeft: -140, marginTop: 28, marginBottom: 20, fontWeight: "bold"}}>Ringkasan Belanja</Text>
                    <View style={{flexDirection: "row", marginBottom: 20}}>
                        <Text style={{color:"white", fontSize: 18, marginLeft: -1, marginTop: -10, fontWeight: "bold"}}>Total Harga(2 Barang)</Text>
                        <Text style={{color:"white", fontSize: 18, marginLeft: 20, marginTop: -10, fontWeight: "bold"}}>Rp. 116.000</Text>
                    </View>
                </View>
                <View style={{
                    alignItems: 'center', 
                    justifyContent: 'center',
                }}>
                
            </View>
            <ScrollView>
            <View style={{
                    backgroundColor: "#4FABC8",
                    height: 40,
                    marginLeft: 30,
                    marginTop: 20,
                    alignItems: 'center', 
                    justifyContent: 'center',
                    width: 300,
                    borderRadius:10,
                }}>
                    <Text style={{color:"white", fontSize: 20, marginLeft: 0, marginTop: 0, textAlign: "center", fontWeight: "bold"}}>Keranjang Belanja Anda</Text>
                </View>
                <View style={{
                    alignItems: 'center', 
                    justifyContent: 'center',
                }}>
                <Text style={{color:"black", fontSize: 20, marginLeft: 0, marginTop: 10, textAlign: "center", fontWeight: "bold"}}>Berhasil menambahkan Ikan Tuna Besar</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
                <Text 
                style={{
                backgroundColor: '#53BD64',
                height: 40,
                width: 300,
                color: '#fff',
                textAlign: "center",
                fontSize: 18,
                borderRadius:10,
                textAlignVertical: "center",
                marginTop: 10,
                }}>Kosongkan Keranjang</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>      
                <Text 
                style={{
                backgroundColor: '#53BD64',
                height: 40,
                width: 300,
                borderRadius:10,
                color: '#fff',
                textAlign: "center",
                fontSize: 18,
                textAlignVertical: "center",
                marginTop: 8,
                }}>Tambah Barang</Text>
            </TouchableOpacity>
            </View>
                <View style={{
                        backgroundColor: "#4FABC8",
                        height: 180,
                        marginLeft: 20,
                        marginTop: 20,
                        alignItems: 'center', 
                        justifyContent: 'center',
                        width: 320,
                        borderRadius:10,
                    }}>
                        
                        <View style={{flexDirection: "row", marginBottom: 20}}>
                        <View style={{
                        marginLeft: 20,
                        }}>
                             <Image source={require('../img/IkanTuna.png')} style={{borderColor:'#4FABC8', borderWidth:2, borderRadius:18, marginLeft:-42}}/>
                            
                            <Text style={{
                        fontSize: 18,
                        marginLeft: 0,
                        marginTop: 20,
                        color: "white",
                        fontWeight: "bold"
                        }}> Sub Total Harga</Text>
                        </View>
                        <View>
                        <Text style={{
                        fontSize: 18,
                        marginLeft: -15,
                        marginTop: 20,
                        color: "white",
                        }}>Ikan Tuna Besar  {"\n"}
                            Rp. 20.000/Kg  </Text>   
                           
                        <Text style={{
                        fontSize: 18,
                        marginLeft: 16,
                        marginTop: 56,
                        color: "white",
                        fontWeight: "bold"
                        }}> Rp. 80.000</Text>
                        </View>
                        
                    </View>
                </View>
                <View style={{
                        backgroundColor: "#4FABC8",
                        height: 180,
                        marginLeft: 20,
                        marginTop: 20,
                        alignItems: 'center', 
                        justifyContent: 'center',
                        width: 320,
                        borderRadius:10,
                    }}>
                        
                        <View style={{flexDirection: "row", marginBottom: 20}}>
                        <View style={{
                        marginLeft: 20,
                        }}>
                             <Image source={require('../img/IkanTuna.png')} style={{borderColor:'#4FABC8', borderWidth:2, borderRadius:18, marginLeft:-25}}/>
                            <Text style={{
                        fontSize: 18,
                        marginLeft: 0,
                        marginTop: 20,
                        color: "white",
                        fontWeight: "bold"
                        }}> Sub Total Harga</Text>
                        </View>
                        <View>
                        <Text style={{
                        fontSize: 18,
                        marginLeft: -0,
                        marginTop: 20,
                        color: "white",
                        }}>Ikan Tuna Medium  {"\n"}
                            Rp. 18.000/Kg  </Text>   
                           
                      
                        <Text style={{
                        fontSize: 18,
                        marginLeft: 16,
                        marginTop: 56,
                        color: "white",
                        fontWeight: "bold"
                        }}> Rp. 36.000</Text>
                        </View>
                        
                    </View>
                    
                </View>
                <View style={{
                        backgroundColor: "#4FABC8",
                        height: 100,
                        marginLeft: 20,
                        marginTop: 20,
                        alignItems: 'center', 
                        justifyContent: 'center',
                        width: 320,
                        borderRadius:10,
                    }}></View>
                </ScrollView>
                
                </ScrollView>
                <View style={{position: 'absolute',backgroundColor:'white', height:100, left: 0, right: 0, bottom: 0}}>
                    <View style={{flexDirection: "row", marginBottom: 20}}>
                        <Text style={{
                        fontSize: 22,
                        marginLeft: 20,
                        marginTop: 20,
                        color: 'black',
                        fontWeight: "bold"
                        }}> Total Harga
                        </Text>
                        <TouchableOpacity onPress={() => navigation.navigate('History')}>      
                            <Text 
                            style={{
                            backgroundColor: '#53BD64',
                            height: 50,
                            width: 160,
                            color: '#fff',
                            textAlign: "center",
                            fontSize: 26,
                            textAlignVertical: "center",
                            marginLeft: 50,
                            marginTop: 20,
                            borderRadius:10,
                            }}>Beli</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{
                        fontSize: 24,
                        marginLeft: 20,
                        marginTop: -50,
                        color: 'black',
                        fontWeight: "bold"
                        }}> Rp. 116.000
                        </Text>
                </View>
        </View>
        
      )
    }
    
    export default Pengiriman;
    