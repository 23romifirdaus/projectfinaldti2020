// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CardView from 'react-native-cardview'
import DetailPesanan from './DetailPesanan';


const DaftarPesanan = ({ navigation }) => {
  return (
      <View style={{paddingTop: 30}}>
         <View>
           <Text style={{textAlign: "center", marginVertical: 20, paddingTop: 50}}>Anda Memiliki 1 Pesanan</Text>
         </View>
         <View>
           <Text style={{textAlign: "left", marginHorizontal: 50}}>Mohon untuk lakukan pembayaran dan upload bukti pembayaran</Text>
         </View>
         <View>
         <View style={{
            height: 50,
            flexDirection: "row",
            backgroundColor: "#4FABC8",
            marginVertical: 30,
            marginHorizontal: 10,
            borderRadius: 5,}}>
            <Text style={{marginRight: 50, margin: 15}}>Ikan Tuna Besar 2 kg</Text>
            <Text style={{marginLeft: 10, margin: 15}}> 13:00 WIB [11/12/2020] </Text>
        </View>
        <Text style={{marginTop: -20, textAlign: "center"}}>Batas Waktu Pembayaran Hingga: {"\n"} 13:00 WIB [12/12/2020] </Text>
        </View>

        <TouchableOpacity onPress={() => navigation.navigate('KonfirmasiPembayaran')}>
          <Text style={{
          backgroundColor: '#4FABC8',
          height: 40,
          color: '#fff',
          textAlign: "center",
          fontSize: 16,
          textAlignVertical: "center",
          marginTop: 30,
          borderRadius: 30,
          marginLeft: 18,
          marginRight: 18}}>
          Upload Bukti Pembayaran
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('DetailPesanan')}>
          <Text style={{
          backgroundColor: '#4FABC8',
          height: 40,
          color: '#fff',
          textAlign: "center",
          fontSize: 16,
          textAlignVertical: "center",
          marginTop: 10,
          borderRadius: 30,
          marginLeft: 18,
          marginRight: 18}}>
          Lihat Detail
          </Text>
        </TouchableOpacity>
      </View>
  );
}

export default DaftarPesanan;