// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CardView from 'react-native-cardview'


const DetailProduk = ({ navigation }) => {
  return (
      <View style={{flex:1}}>
        <View style={{ marginTop: 0}}>
        <Image style={{resizeMode: 'stretch',width:300,height:200, marginLeft:30}}source={require('../img/IkanTuna.png')}/>
        </View>
        <View style={{flexDirection: "column", justifyContent: "center", backgroundColor: "#4FABC8",marginTop:0, marginLeft:20,borderRadius:10, marginRight:20 }}>
        <Text style={{
          fontSize: 32,
          fontWeight: "bold",
          color: 'white',
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 0,
          borderRadius:10,
          marginBottom: 10,
          marginLeft: 18,
          borderRadius:10,
          marginRight: 18}}>
          Ikan Tuna Besar
          </Text>
          </View>
          <View style={{flexDirection: "row", justifyContent: "center", backgroundColor: "#4FABC8",borderRadius:10, margin: 20 }}>
          <Text style={{
          color: 'white',
          fontWeight:'bold',
          margin: 10,
          fontSize:18,
          }}>
          Stok                           100kg {"\n"}
          Ukuran                      20cm x 10cm {"\n"}
          Diambil Dari             Pelabuhan Tegal{"\n"}
          Harga                        Rp 20.000/kg{"\n"}
          {"\n"}Ikan cocok untuk bakar-bakaran {"\n"}
            dan tahan sampai 10 hari
          </Text>
        </View> 
        <View style={{flexDirection: "column", justifyContent: "center", backgroundColor: "#4FABC8",marginTop:40 }}>
        <Text style={{
          color: 'white',
          fontWeight: "bold",
          textAlign: "center",
          textAlignVertical: "center",
          marginTop: 10,
          fontSize:18,
        //  marginBottom: 10,
          marginLeft: 10,
          marginRight: 10}}>
          Masukan Jumlah Yang Anda Inginkan{"\n"}
          </Text>
          </View>
          <View style={{flexDirection: "row",justifyContent: "center", backgroundColor: "#4FABC8"}}>
          <TextInput
          placeholder="kg"
          style={{
            height : 40, 
            fontSize: 18,
            textAlign: 'center',
            width: 150,
            borderColor: '#4FABC8', 
            borderWidth: 2,
            borderBottomWidth: 2,
            borderTopLeftRadius: 10,
            borderBottomLeftRadius:10,
            marginLeft: 18,
            marginBottom:10,
            backgroundColor: 'white',
          }}
          />
          <TouchableOpacity onPress={() => navigation.navigate('KeranjangBelanja')}>
          <Text style={{
          backgroundColor: '#4FABC8',
          height: 40,
          width : 80,
          color: '#fff',
          textAlign: "center",
          textAlignVertical: "center",
          marginBottom: 10,
          borderTopRightRadius: 10,
          borderBottomRightRadius:10,
          marginRight: 18,
          backgroundColor: '#53BD64',}}>
          Pesan
          </Text>
          </TouchableOpacity>
        </View>
      </View>
  );
}

export default DetailProduk;