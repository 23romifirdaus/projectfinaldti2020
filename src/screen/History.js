// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CardView from 'react-native-cardview'

const styles = StyleSheet.create({
  container: {
  marginVertical: 10
  },
});

const History = ({ navigation }) => {
  return (
    <View style={{flexDirection: "column" }}>
    <View style={{flexDirection: "row"}}>
     <Text style={{
     marginTop: 10,
     marginLeft: 18,
     marginRight: 18,
     backgroundColor: '#4FABC8', 
     borderRadius: 8,
     height: 100,
     width: 330,
     color: '#fff',
     textAlign: "left",
     fontSize: 16,
     textAlignVertical: "center",
     padding: 10,  
     }}>
     Tanggal Pesan : 30/11/2020 {"\n"}
     Ikan Tuna Besar 4kg                          Dikirim {"\n"}
     Rp 80.000
     <TouchableOpacity onPress={() => navigation.navigate('DetailTransaksiDikirim')}>
     <Text style={{
     backgroundColor: '#53BD64',
     color: '#fff',
     textAlign: "center",
     textAlignVertical: "center",
     marginTop: 10,
     borderRadius: 10,
     height:30,
     marginLeft:60,
     }}>
    Lihat Detail Transaksi Anda
     </Text>
     </TouchableOpacity>
     </Text>
   </View>
   <View style={{flexDirection: "row"}}>
     <Text style={{
       marginTop: 10,
       marginLeft: 18,
       marginRight: 18,
       backgroundColor: '#4FABC8', 
       borderRadius: 8,
       height: 100,
       width: 330,
       color: '#fff',
       textAlign: "left",
       fontSize: 16,
       textAlignVertical: "center",
       padding: 10,  
     }}>
     Tanggal Pesan : 25/11/2020 {"\n"}
     Ikan Tuna Besar 4kg                          Selesai {"\n"}
     Rp 80.000
     <TouchableOpacity onPress={() => navigation.navigate('DetailTransaksiSelesai')}>
     <Text style={{
     backgroundColor: '#53BD64',
     color: '#fff',
     textAlign: "center",
     textAlignVertical: "center",
     marginTop: 10,
     borderRadius: 10,
     height:30,
     marginLeft:60,
     }}>
    Lihat Detail Transaksi Anda
     </Text>
     </TouchableOpacity>
     </Text>
   </View>
   <View style={{flexDirection: "row"}}>
     <Text style={{
       marginTop: 10,
       marginLeft: 18,
       marginRight: 18,
       backgroundColor: '#4FABC8', 
       borderRadius: 8,
       height: 100,
       width: 330,
       color: '#fff',
       textAlign: "left",
       fontSize: 16,
       textAlignVertical: "center",
       padding: 10,  
     }}>
     Tanggal Pesan : 24/11/2020 {"\n"}
     Ikan Tuna Besar 2kg                    Dibatalkan {"\n"}
     Rp 40.000
     <TouchableOpacity onPress={() => navigation.navigate('DetailTransaksiDibatalkan')}>
     <Text style={{
     backgroundColor: '#53BD64',
     color: '#fff',
     textAlign: "center",
     textAlignVertical: "center",
     marginTop: 10,
     borderRadius: 10,
     height:30,
     marginLeft:60,
     }}>
    Lihat Detail Transaksi Anda
     </Text>
     </TouchableOpacity>
     </Text>
   </View>
 </View>

  );
}

export default History;