// In App.js in a new project

import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  TouchableHighlight,
  TouchableOpacityComponent
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CardView from 'react-native-cardview'
import DropDownPicker from 'react-native-dropdown-picker';


const KonfirmasiPemesanan = ({ navigation }) => {
  return (
      <View>
          <View>
              <Text style={{fontWeight:"bold", fontSize: 18, marginTop: 20, marginLeft: 25}}>
                  Nama Penerima
              </Text>
              <TextInput
              placeholder="Nama Penerima"
              style={{
              height: 40,
              padding: 10,
              marginTop: 5,
              marginLeft: 20,
              marginRight: 20,
              borderColor: '#A9A9A9', 
              borderWidth: 1,
              borderBottomWidth: 1,
              borderRadius: 7
                }}/>
            <Text style={{fontWeight:"bold", fontSize: 18, marginTop: 10, marginLeft: 25}}>
                  Alamat
            </Text>
          
            <Text style={{fontWeight:"bold", fontSize: 18, marginTop: 10, marginLeft: 25}}>
                  No. Handphone
            </Text>
                <TextInput
              placeholder="Nomor Handphone"
              keyboardType="numeric"
              style={{
                height: 40,
                padding: 10,
                marginTop: 5,
                marginLeft: 20,
                marginRight: 20,
                borderColor: '#A9A9A9', 
                borderWidth: 1,
                borderBottomWidth: 1,
                borderRadius: 7
                }}/>
            <Text style={{fontWeight:"bold", fontSize: 18, marginTop: 10, marginLeft: 25}}>
                  Metode Pembayaran
            </Text>
                 <TextInput
              placeholder="Metode Pembayaran"
              style={{
                height: 40,
                padding: 10,
                marginTop: 5,
                marginLeft: 20,
                marginRight: 20,
                borderColor: '#A9A9A9', 
                borderWidth: 1,
                borderBottomWidth: 1,
                borderRadius: 7
                }}/>
            </View>
            <Text style={{textAlign: "center",margin: 10}}>Pastikan Anda Mengisinya dengan Benar</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Pembayaran')}>
                <Text style={{backgroundColor: "#53BD64", textAlign: "center",marginLeft: 70, marginRight:70, padding: 10, borderRadius: 30, color: "white"}}>Pesan Sekarang</Text>
            </TouchableOpacity>
      </View>
  );
}

export default KonfirmasiPemesanan;