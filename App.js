import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  FlatList, 
  TouchableOpacity,
  BackHandler
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import SplashScreen from './src/screen/SplashScreen';
import Home from './src/screen/Home';
import Registrasi from './src/screen/Registrasi';
import Login from './src/screen/Login';
import History from './src/screen/History';
import Settings from './src/screen/Settings';
import UbahProfile from './src/screen/UbahProfile';
import Keluhan from './src/screen/Keluhan';
import GantiPassword from './src/screen/GantiPassword';
import About from './src/screen/About';
import DaftarPesanan from './src/screen/DaftarPesanan';
import DetailPesanan from './src/screen/DetailPesanan';
import DetailProduk from './src/screen/DetailProduk';
import Pemesanan from './src/screen/Pemesanan';
import KonfirmasiPemesanan from './src/screen/KonfirmasiPemesanan';
import Pembayaran from './src/screen/Pembayaran';
import KonfirmasiPembayaran from './src/screen/KonfirmasiPembayaran';
import DetailTransaksiSelesai from './src/screen/DetailTransaksiSelesai';
import DetailTransaksiDikirim from './src/screen/DetailTransaksiDikirim';
import Invoice from './src/screen/Invoice';
import BatalkanPesanan from './src/screen/BatalkanPesanan';
import DetailTransaksiDibatalkan from './src/screen/DetailTransaksiDibatalkan';
import Alamat from './src/screen/Alamat';
import KeranjangKosong from './src/screen/KeranjangKosong';
import KeranjangBelanja from './src/screen/KeranjangBelanja';
import Pengiriman from './src/screen/Pengiriman';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const IconBottom = (props) => {
  const { color, focused } = props.data
  let colorSelected = focused ? color : 'grey'
  return (
      <View>
          <Image source={props.image} style={{ width: 40, height: 40, tintColor: colorSelected }} />
      </View>
  )
}

function SplashStack() {
  return (
      <Stack.Navigator
        initialRouteName="Splash"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Splash"
          component={SplashScreen}
          options={{headerShown: false}}/>
      </Stack.Navigator>
  );
}

function HomeStack() {
  return (
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}/>
      </Stack.Navigator>
  );
}

function LoginStack() {
  return (
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}/>
      </Stack.Navigator>
  );
}

function KeranjangKosongStack() {
  return (
      <Stack.Navigator
        initialRouteName="KeranjangKosong"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8'},
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen
          name="KeranjangKosong"
          component={KeranjangKosong}
          options={{headerShown: false}}/>
      </Stack.Navigator>
  );
}

function KeranjangBelanjaStack() {
  return (
      <Stack.Navigator
        initialRouteName="KeranjangBelanja"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8'},
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen
          name="KeranjangBelanja"
          component={KeranjangBelanja}
          options={{headerShown: false}}/>
      </Stack.Navigator>
  );
}

function RegistrasiStack() {
  return (
      <Stack.Navigator
        initialRouteName="Registrasi"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8'},
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen
          name="Registrasi"
          component={Registrasi}
          options={{headerShown: false}}/>
      </Stack.Navigator>
  );
}

function PengirimanStack() {
  return (
      <Stack.Navigator
        initialRouteName="Pengiriman"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8'},
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen
          name="Pengiriman"
          component={Pengiriman}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function HistoryStack() {
  return (
      <Stack.Navigator
        initialRouteName="History"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen
          name="Riwayat Transaksi"
          component={History}/>
      </Stack.Navigator>
  );
}

function SettingsStack() {
  return (
      <Stack.Navigator
        initialRouteName="Profile"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Profile"
          component={Settings}
          options={{ title: 'Profile'}}/>
      </Stack.Navigator>
  );
}

function UbahProfileStack() {
  return (
      <Stack.Navigator
        initialRouteName="UbahProfile"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Ubah Profile"
          component={UbahProfile}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function KeluhanStack() {
  return (
      <Stack.Navigator
        initialRouteName="Keluhan"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Lapor Keluhan"
          component={Keluhan}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function GantiPasswordStack() {
  return (
      <Stack.Navigator
        initialRouteName="GantiPassword"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Ganti Password"
          component={GantiPassword}
          options={{headerShown: false}}/>
      </Stack.Navigator>
  );
}

function AboutStack() {
  return (
      <Stack.Navigator
        initialRouteName="About"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="About"
          component={About}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function DaftarPesananStack() {
  return (
      <Stack.Navigator
        initialRouteName="DaftarPesanan"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Daftar Pesanan"
          component={DaftarPesanan}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function DetailPesananStack() {
  return (
      <Stack.Navigator
        initialRouteName="DetailPesanan"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Detail Pesanan"
          component={DetailPesanan}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function DetailProdukStack() {
  return (
      <Stack.Navigator
        initialRouteName="DetailProduk"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Detail Produk"
          component={DetailProduk}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function PemesananStack() {
  return (
      <Stack.Navigator
        initialRouteName="Pemesanan"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Pemesanan"
          component={Pemesanan}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function KonfirmasiPemesananStack() {
  return (
      <Stack.Navigator
        initialRouteName="KonfirmasiPemesanan"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Konfirmasi Pemesanan"
          component={KonfirmasiPemesanan}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function PembayaranStack() {
  return (
      <Stack.Navigator
        initialRouteName="Pembayaran"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Pembayaran"
          component={Pembayaran}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}
function KonfirmasiPembayaranStack() {
  return (
      <Stack.Navigator
        initialRouteName="KonfirmasiPembayaran"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="KonfirmasiPembayaran"
          component={KonfirmasiPembayaran}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function DetailTransaksiDikirimStack() {
  return (
      <Stack.Navigator
        initialRouteName="DetailTransaksiDikirim"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Detail Transaksi"
          component={DetailTransaksiDikirim}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function DetailTransaksiSelesaiStack() {
  return (
      <Stack.Navigator
        initialRouteName="DetailTransaksiSelesai"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Detail Transaksi"
          component={DetailTransaksiSelesai}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function InvoiceStack() {
  return (
      <Stack.Navigator
        initialRouteName="Invoice"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Detail Invoice"
          component={Invoice}
          options={{headerBackTitle: null}}/>
      </Stack.Navigator>
  );
}

function BatalkanPesananStack() {
  return (
      <Stack.Navigator
        initialRouteName="BatalkanPesanan"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Batalkan Pemesanan"
          component={BatalkanPesanan}
          options={{headerShown: false}}/>
      </Stack.Navigator>
  );
}

function DetailTransaksiDibatalkanStack() {
  return (
    <Stack.Navigator
    initialRouteName="DetailTransaksiDIbatalkan"
    screenOptions={{
      headerStyle: { backgroundColor: '#4FABC8' },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
    }}>
    <Stack.Screen
      name="Detail Transaksi"
      component={DetailTransaksiDibatalkan}
      options={{headerBackTitle: null}}/>
  </Stack.Navigator>
  );
}

function AlamatStack() {
  return (
      <Stack.Navigator
        initialRouteName="Alamat"
        screenOptions={{
          headerStyle: { backgroundColor: '#4FABC8' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
        }}>
        <Stack.Screen
          name="Lihat Alamat"
          component={Alamat}/>
      </Stack.Navigator>
  );
}

function HomeTabs() {
  return (

    
    <Tab.Navigator tabBarOptions={{
      activeTintColor: '#4FABC8',
      style: { height: 60}
  }}
  >
      <Tab.Screen name="Home" component={HomeStack}
                options={{
                    title: "Home",

                    tabBarIcon: (props) => (
                        <IconBottom data={props} image={require('./src/img/icon_home.png')}/>
                    )
                }}
            />
      <Tab.Screen name="History" component={HistoryStack}
                options={{
                    title: "History",
                    tabBarIcon: (props) => (
                        <IconBottom data={props} image={require('./src/img/icon_history.png')} />
                    )
                }} />
      <Tab.Screen name="KeranjangKosong" component={KeranjangKosongStack}
                options={{
                    title: "Daftar Pesanan",
                    tabBarIcon: (props) => (
                        <IconBottom data={props} image={require('./src/img/icon_cart.png')} />
                    )
                }} />
      <Tab.Screen name="About" component={AboutStack}
                options={{
                    title: "About",
                    tabBarIcon: (props) => (
                        <IconBottom data={props} image={require('./src/img/icon_about.png')} />
                    )
                }} />
      <Tab.Screen name="Profile" component={SettingsStack}
                options={{
                    title: "Profile",
                    tabBarIcon: (props) => (
                        <IconBottom data={props} image={require('./src/img/Profile-PNG-Icon.png')} />
                    )
                }} />
    </Tab.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Splash" component={SplashStack} options={{headerShown: false}}/>
      <Stack.Screen name="Home" component={HomeTabs} options={{headerShown: false}}/>
      <Stack.Screen name="Login" component={LoginStack} options={{headerShown: false}}/>
      <Stack.Screen name="Registrasi" component={RegistrasiStack} options={{
          title: 'Registrasi Akun',
          headerStyle: {
          backgroundColor: '#4FABC8',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}/>
      <Stack.Screen name="UbahProfile" component={UbahProfileStack} options={{headerShown: false}}/>
      <Stack.Screen name="Pengiriman" component={PengirimanStack} options={{headerShown: false}}/>
      <Stack.Screen name="KeranjangBelanja" component={KeranjangBelanjaStack} options={{headerShown: false}}/>
      <Stack.Screen name="Keluhan" component={KeluhanStack} options={{headerShown: false}}/>
      <Stack.Screen name="GantiPassword" component={GantiPasswordStack} options={{headerShown: false}}/>
      <Stack.Screen name="About" component={AboutStack} options={{headerShown: false}}/>
      <Stack.Screen name="DetailProduk" component={DetailProdukStack} options={{headerShown: false}}/>
      <Stack.Screen name="DetailPesanan" component={DetailPesananStack} options={{headerShown: false}}/>
      <Stack.Screen name="Pemesanan" component={PemesananStack} options={{headerShown: false}}/>
      <Stack.Screen name="KonfirmasiPemesanan" component={KonfirmasiPemesananStack} options={{headerShown: false}}/>
      <Stack.Screen name="Pembayaran" component={PembayaranStack} options={{headerShown: false}}/>
      <Stack.Screen name="KonfirmasiPembayaran" component={KonfirmasiPembayaranStack} options={{headerShown: false}}/>
      <Stack.Screen name="DetailTransaksiDikirim" component={DetailTransaksiDikirimStack} options={{headerShown: false}}/>
      <Stack.Screen name="DetailTransaksiSelesai" component={DetailTransaksiSelesaiStack} options={{headerShown: false}}/>
      <Stack.Screen name="Invoice" component={InvoiceStack} options={{headerShown: false}}/>
      <Stack.Screen name="BatalkanPesanan" component={BatalkanPesananStack} options={{headerShown: false}}/>
      <Stack.Screen name="DetailTransaksiDibatalkan" component={DetailTransaksiDibatalkanStack} options={{headerShown: false}}/>
      <Stack.Screen name="Alamat" component={AlamatStack} options={{headerShown: false}}/>
    </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;